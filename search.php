<?php get_header(); ?>

<?php echo do_shortcode('[header]') ?>

<div class="page-title" style="display: block; padding: 60px; background: #f1f1f1; margin-bottom: 30px">
  <div class="container">
      <h1><?php  echo gett('Resultados de búsqueda:') . " " . get_search_query(); ?></h1>
  </div>
</div>

<div class="container">
<?php echo do_shortcode("[posts search=' " . get_search_query() . "']")  ?>

            <?php if ( have_posts() ) : ?>

            <?php else : ?>

            <?php endif; ?>

</div><!-- #content .site-content -->

<?php get_footer(); ?>