import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import multipleRender from "react-multiple-render";
import FullHeightHero from './components/full-height-hero';
import Header from './components/header';
import LeadForm from './components/lead-form';
import CommentForm from './components/comment-form';
import contactMap from './components/contact-map';
import InstagramItem from './components/instagram-item';
import ProcedureItem from './components/procedure-item';
import ProcedureContent from './components/procedure-content';
import Slider from './components/slider';
import Posts from './components/posts';
import YtSlider from './components/yt-slider';
import SocialBanner from './components/social-banner';
import NewsletterForm from './components/newsletter-form';
import Cta from './components/cta';
import Testimonial from './components/testimonial';
import Testimonials from './components/testimonials';

multipleRender(FullHeightHero, '.full-height-hero-container');
multipleRender(Header, '.header-container');
multipleRender(LeadForm, '.form-container');
multipleRender(contactMap, '.contact-map-container');
multipleRender(InstagramItem, '.instagram-item-container');
multipleRender(ProcedureItem, '.procedure-item-container');
multipleRender(ProcedureContent, '.procedure-content-container');
multipleRender(Slider, '.slider-container');
multipleRender(Posts, '.posts-container');
multipleRender(YtSlider, '.yt-slider-container');
multipleRender(SocialBanner, '.social-banner-container');
multipleRender(NewsletterForm, '.newsletter-form-container');
multipleRender(CommentForm, '.comment-form-container');
multipleRender(Cta, '.cta-container');
multipleRender(Testimonial, '.testimonial-container');
multipleRender(Testimonials, '.testimonials-container');

jQuery('a').on('click', function(e) {
  const hash = this.hash;
  if (hash !== '') {
    e.preventDefault();

    const less = jQuery('.header-container') ? jQuery('.header-container').height() : 0;

    const scrollTop = jQuery(hash) ? (jQuery(hash).offset().top - less) : 0;
    jQuery('html, body').animate({ scrollTop }, 800, () => {});
  }
});

// jQuery(window).on('scroll', function(e) {
//   jQuery('.header__menu li a').each(function() {
//     if(window.pageYOffset <= jQuery(jQuery(this).attr("href"))) {
//       console.log('in section', jQuery(jQuery(this).attr("href")));
//     }

//     console.log('out section');
//   });
// });