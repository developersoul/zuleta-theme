import React, { Component } from 'react';

class Cta extends Component {

  render() {
    const { url, text, trans } = this.props;

    return (
      <div className="cta">
        <div className="cta__text">
          <h3>{text}</h3>
        </div>
        <a href={url} className="btn cta-btn">{trans.btn_text}</a>
      </div>
    )
  }
}

export default Cta;