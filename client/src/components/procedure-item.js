import React, { Component } from 'react';
import emmiter from 'tiny-emitter/instance';
class ProcedureItem extends Component {

  openContent = (e) => {
    e.preventDefault();
    emmiter.emit('ProcedureSetContent', this.props.categories);
  }

  render() {
    const { image_url, title, url } = this.props;

    return (
      <a href={url} className="procedure-item">
        <img src={image_url} />
        <h4>{title}</h4>
      </a>
    )
  }
}

export default ProcedureItem;