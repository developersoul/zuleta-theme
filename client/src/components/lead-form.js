import React, { Component } from 'react';
import { validateEmail, validateEmpty } from '../lib/validate';

class Form extends Component {

  state = {
    country: this.props.country,
    city: '',
    name: '',
    email: ''
  }

  componentDidMount() {
    this.setState({ country: window.country });
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    if(validateEmail(this.state.email)) {
      console.log('form data', this.state);
    } else {

    }
  }

  render() {
    const { trans, countries } = this.props;
    const { country, city, name, email } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input
            name="name"
            value={name}
            onChange={this.handleChange}
            type="text"
            className="form-control"
            placeholder={trans.name} />
        </div>
        <div className="form-group">
          <input
            name="email"
            onChange={this.handleChange}
            value={email}
            type="text"
            className="form-control"
            placeholder={trans.email} />
        </div>
        <div className="form-group">
          <button className="btn cta-btn" style={{ float: 'right' }}>{trans.btn}</button>
        </div>
      </form>
    )
  }
}

export default Form;