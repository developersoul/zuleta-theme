import slick from 'slick-carousel';
import React, { Component } from 'react';
import jQuery from 'jquery';

class Testimonials extends Component {

	componentDidMount() {
		jQuery(this.slider).slick({
			autoplay: true,
      arrows: false,
      dots: true,
      adaptiveHeight: true,
      appendDots: this.dots
		});
	}

	render() {
		const { testimonials } = this.props;

    return (
      <section className="testimonials">
        <div ref={ref => this.slider = ref}>
          {testimonials.map(testimonial => {
            return (
              <div className="testimonials-item">
                <div className="testimonials-item__content">
                  <h5>{testimonial.text}</h5>
                  <span>{testimonial.author}</span>
                </div>
                <img src={testimonial.image} />
              </div>
            )
          })}
        </div>
        <div ref={ref => this.dots = ref} className="testimonials__dots"></div>
      </section>
    )
  }
};

export default Testimonials;