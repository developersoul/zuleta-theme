import React, { Component } from 'react';
import { validateEmail, validateEmpty } from '../lib/validate';
import request from 'axios';
import qs from 'qs';

class CommentForm extends Component {

  state = {
    comment_content: '',
    comment_author: '',
    comment_author_email: '',
    spam: '',
    success: false
  }

  handleSubmit = (e) => {
    if(e) e.preventDefault();

    if(this.state.spam.length == 0) {
      const reqData = qs.stringify({ action: 'store_comment', data: this.state });
      request.post('/wp-admin/admin-ajax.php', reqData).then((res) => {
        if(res.data.id) this.setState({ success: true });

        this.setState({
          comment_content: '',
          comment_author: '',
          comment_author_email: '',
        });
      })
    }
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  render() {
    const {
      comment_content,
      comment_author,
      comment_author_email,
      spam,
      success,
    } = this.state;
    const { trans } = this.props;

    return (
      <form className="comment-form" onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label htmlFor="">{trans.comment}</label>
          <textarea
            name="comment_content"
            className="form-control"
            rows="8"
            onChange={this.handleChange}
            value={comment_content}
          />
        </div>
        <div className="row">
          <div className="col-lg-6">
            <div className="form-group">
              <label htmlFor="">{trans.name}</label>
              <input
                name="comment_author"
                type="text"
                className="form-control"
                onChange={this.handleChange}
                value={comment_author}
              />
            </div>
          </div>
          <div className="col-lg-6">
            <div className="form-group">
            <label htmlFor="">{trans.email}</label>
              <input
                name="comment_author_email"
                type="text"
                className="form-control"
                onChange={this.handleChange}
                value={comment_author_email}
                />
              <input
                name="url"
                type="hidden"
                onChange={this.handleChange}
                value={spam}
              />
            </div>
          </div>
        </div>
        {success &&
          <div className="form-group">
            <div className="alert alert-success">{trans.comment_send}</div>
          </div>
        }
        <div className="form-group">
          <button className="btn">{trans.post_comment}</button>
        </div>
      </form>
    )
  }
}

export default CommentForm;