import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

class ContactMap extends Component {

  static defaultProps = {
    center: {
      lat: 10.4005611,
      lng: -75.5586051
    },
    zoom: 15
  }

  render() {
    const { city, address, phone, trans } = this.props;
    let center = {
      lat: parseFloat(this.props.center.lat),
      lng: parseFloat(this.props.center.lng)
    };

    return (
      <section className="contact-map">
        <div className="contact-map__header">
          <ul>
            <li style={{ fontWeight: 'bold' }}>{city}</li>
            <li>{address}</li>
            <li>{phone}</li>
          </ul>
          <a
            href={`tel:${phone}`}
            className="btn btn-default contact-map__cta">
            <i className="fas fa-phone"></i> {trans.cta_text}
          </a>
        </div>
        {this.props.center.lat.length > 0 && this.props.center.lng.length > 0 ?
        <div className="contact-map__map">
          <GoogleMapReact
            bootstrapURLKeys={{ key: 'AIzaSyB4bwv7inF4VBZK0_zmIX1d0fqmgsdrBpc' }}
            defaultCenter={center}
            defaultZoom={this.props.zoom}
          >
          <span
            lat={this.props.center.lat}
            lng={this.props.center.lng}
          >
            <i className="ion-ios-location" style={{ fontSize: '50px' }}></i>
          </span>
          </GoogleMapReact>
        </div>
        : null}
      </section>
    );
  }
}

export default ContactMap;