import React, { Component } from 'react';
import emmiter from 'tiny-emitter/instance';
import Form from './lead-form';

class ProcedureContent extends Component {
  state = {
    categories: [],
    category: {}
  }

  componentDidMount() {
    emmiter.on('ProcedureSetContent', this.setContent);
  }

  componentWillUnmount() {
    emmiter.off('ProcedureSetContent', this.setContent);
  }

  setContent = (categories) => {
    if(categories.length > 0) {
      this.setState({ categories, category: categories[0] });
    }

  }

  changeCategory = (e, category) => {
    if(e) e.preventDefault();
    this.setState({ category });
  }

  render() {

    const { categories, category } = this.state;

    if(categories.length == 0) return null;

    return (
      <section className="procedure-content">
        <div className="row">
          <div className="col-lg-4 col-md-4 eq-hw">
            <div className="procedure-content__list">
              <ul>
                {categories.map(cat =>
                    <li>
                      <a
                        href="#"
                        onClick={(e) => this.changeCategory(e, cat)}
                        className={category.category_name == cat.category_name ? 'procedure-content__list-active' : ''}
                      >{cat.category_name}</a>
                    </li>
                  )}
              </ul>
            </div>
          </div>
          <div className="col-lg-8 col-md-8">
            <div className="procedure-content__content">
            {Object.keys(category).length > 0 &&
              <span>
                <h3>{category.category_name}</h3>
                <p>{category.category_text}</p>
              </span>
            }
            </div>
            <div className="procedure-content__form">
              <h4>{this.props.trans.form_text}</h4>
              <Form {...this.props} />
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default ProcedureContent;