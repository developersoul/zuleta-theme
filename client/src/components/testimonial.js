import React, { Component } from 'react';

class Testimonial extends Component {

  render() {
    const { imgUrl, text, author } = this.props;

    return (
      <div className="testimonial">
        <div className="row">
          <div className="col-lg-4">
            <div className="testimonial__content">
              <p>{text}</p>
              <span>{author}</span>
            </div>
          </div>
          <div className="col-lg-8">
            <img src={imgUrl} alt=""/>
          </div>
        </div>
      </div>
    )
  }
}

export default Testimonial;