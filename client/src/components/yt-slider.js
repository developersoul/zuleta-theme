import React, { Component } from 'react';
import * as UUID from "uuidjs";

class YoutubePlayer extends Component {
  state = {
    uid: UUID.generate()
  }

  componentDidMount() {

    const onYouTubeIframeAPIRead = () => {
      this.player = new YT.Player(`player-${this.state.uid}`, {
        height: '390',
        width: '640',
        videoId: this.props.videoId,
        events: {
          'onReady': this.onPlayerReady,
          'onStateChange': this.onPlayerStateChange
        }
      });
    }

    YT.ready(() => {
      onYouTubeIframeAPIRead();
    });

  }

  onPlayerReady = () => {

  }

  onPlayerStateChange = (e) => {
    if(e.data === 0) {
      this.props.next();
    }
  }

  render() {
    return (
      <div className="iframe-container">
        <div className="video-iframe" id={`player-${this.state.uid}`}></div>
      </div>
    )
  }
}

class YoutubeSlider extends Component {

  state = {
    current: 0,
    height: 600
  }

  componentDidMount() {
    const { slides } = this.props;
    this.total = slides.length;
    this.setState({ height: this.slide.offsetHeight });
  }

  next = (e) => {
    if(e) e.preventDefault();

    if(this.state.current < this.total - 1) {
      this.setState({ current: this.state.current + 1 });
    } else {
      this.setState({ current: 0 });
    }
  }

  prev = (e) => {
    if(e) e.preventDefault();

    if(this.state.current < this.total && this.state.current > 0)  {
      this.setState({ current: this.state.current - 1 });
    }
  }

  render() {
    const { slides } = this.props;

    return (
      <section className="yt-slider" style={{ height: this.state.height }}>
      {slides.map((slide, i) =>
        <div
          ref={ref => this.slide = ref}
          className={this.state.current == i ? "yt-slider__slide yt-slider__slide-active" : "yt-slider__slide"}
        >
          <div className="yt-slider__slide-container">
            <span className="yt-slider__title"><h5>{slide.title}</h5></span>
            <YoutubePlayer next={this.next} videoId={slide.yt_id} />
          </div>
        </div>
      )}
      <button
        onClick={this.prev}
        className="yt-slider__btn yt-slider__btn-prev">

      </button>
      <button
        onClick={this.next}
        className="yt-slider__btn yt-slider__btn-next">

        </button>
      </section>
    )
  }
}

export default YoutubeSlider;