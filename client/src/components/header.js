import React, { Component } from 'react';

class Header extends Component {

  state = {
    sticky: false,
    offsetTop: 0,
    showMenu: false
  }

  componentDidMount() {
    this.headerOffset = this.header.offsetParent.offsetTop;

    window.onscroll = () => {
      this.onScroll();
    }
  }

  onScroll = () => {
    if (window.pageYOffset >= this.headerOffset) {

      this.setState({ sticky: true });
    } else {
      this.setState({ sticky: false });
    }
  }

  toggleMenu = (e) => {
    e.preventDefault();
    this.setState({ showMenu: !this.state.showMenu });
  }

  render() {
    const { menu, logo } = this.props;
    const { sticky, showMenu } = this.state;

    return (
      <div className={sticky ? "header header__sticky" : "header"} ref={ref => this.header = ref}>
        <div className="container">
          <div className="header__container">
            <a className="header__logo" href="/"><img src={logo} alt=""/></a>
            <ul className="header__menu" dangerouslySetInnerHTML={{__html: menu}} />
            <ul className={showMenu ? "header__menu-mobile header__menu-mobile--open" : "header__menu-mobile"}>
              <a
                href="#"
                className="header__closer"
                onClick={this.toggleMenu}>
                <i className="fas fa-times"></i>
              </a>
              <span dangerouslySetInnerHTML={{__html: menu}} />
            </ul>
            <a
              href="#"
              className="header__opener"
              onClick={this.toggleMenu}>
              <i className="fas fa-bars"></i>
            </a>
          </div>

        </div>
      </div>
    )
  }
}

export default Header;