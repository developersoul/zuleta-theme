const Path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: {
		vendor: [
			'react',
			'react-dom',
			'react-multiple-render',
			'jquery',
			'slick-carousel'
    ],
		app: './src/app.js'
	},
	output: {
		path: Path.join(__dirname, '/dist/'),
    filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			}
		]
	},
	// optimization: {
	// 	splitChunks: {
	// 	  chunks: 'all'
	// 	}
	// }
};
