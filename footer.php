<section class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <img src="<?php echo get_template_directory_uri() ?>/client/img/logo_footer.png" class="footer__logo">
      </div>

      <div class="col-lg-8">
        <ul class="footer__menu">
          <?php
            $args = array(
              'theme_location' => 'header',
              'container' => false,
              'echo' => false
            );

            $menu = wp_nav_menu( $args);
            echo clean_menu($menu);
          ?>
          <li class="menu-social">
            <a href="#fb"><i class="fab fa-facebook-f"></i></a>
          </li>
          <li class="menu-social">
          <a href="#fb"><i class="fab fa-instagram"></i></a>
          </li>
          <li class="menu-social">
          <a href="#fb"><i class="fab fa-whatsapp"></i></a>
          </li>
        </ul>
        <div class="row">
          <div class="col-lg-6">
          <?php dynamic_sidebar('footer_left') ?>
          </div>
          <div class="col-lg-6">
          <?php dynamic_sidebar('footer_right') ?>

          </div>
        </div>

      </div>

    </div>
  </div>
</section>

<script src="http://www.youtube.com/player_api"></script>
<script src="<?php echo get_template_directory_uri() ?>/client/dist/vendor.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/client/dist/app.js?v=<?php echo filemtime(get_template_directory() . '/client/dist/app.js') ?>"></script>
<?php wp_footer() ?>
<script>
  window.country = "<?php echo getCountry(); ?>";
  window.city = "<?php echo getCity(); ?>";
</script>
</body>
</html>

