<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>DR Carlos Zuleta Bechara - <?php echo get_the_title(); ?></title>

  <meta name="theme-color" content="#111111">
  <meta property="title" content="DR Carlos Zuleta Bechara - <?php echo get_the_title(); ?>" />

  <meta property="og:title" content="DR Carlos Zuleta Bechara - <?php echo get_the_title(); ?>" />
  <meta property="og:url" content="<?php echo the_permalink() ?>" />
  <meta property="og:image" content="<?php echo get_the_post_thumbnail_url() ?>" />
  <meta property="og:description" content="<?php echo get_the_excerpt(); ?>" />

  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="<?php echo get_the_title(); ?>">
  <meta name="twitter:image" content="<?php echo get_the_post_thumbnail_url() ?>">
  <meta name="twitter:description" content="<?php echo get_the_excerpt(); ?>" />

  <link
    rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
  <link
    rel="stylesheet"
    href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
    integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
    crossorigin="anonymous">
  <link
    rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Lato:300,400,700|PT+Sans:400,700">
  <link
    rel="stylesheet"
    href="<?php echo get_template_directory_uri() ?>/client/dist/index.css?v=<?php echo filemtime(get_template_directory() . '/client/dist/index.css') ?>">
  <link
    rel="stylesheet"
    type="text/css"
    href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
  <link
    rel="stylesheet"
    type="text/css"
    href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
  <?php wp_head() ?>
</head>
<body>

