<?php

function cta_sc( $atts ){

	$at = shortcode_atts([
    'text' => '',
	'url' => '',
	'btn_text' => gett('AGENDA TU CITA HOY MISMO')
	], $atts);

	$props = [
		"text" => $at['text'],
		"url" => $at['url'],
		"trans" => [
			"btn_text" => $at['btn_text']
		]
	];

	ob_start();
	?>

	<div
    class="cta-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'cta', 'cta_sc' );