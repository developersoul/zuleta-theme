<?php

function header_sc( $atts ){

	$at = shortcode_atts([
		'logo' => '',
	], $atts);

	$args = array(
		'theme_location' => 'header',
		'container' => false,
		'echo' => false
	);

	$menu = wp_nav_menu( $args);


	$props = [
		"logo" => empty($at['logo']) ? get_template_directory_uri() . '/client/img/logo_header.png' : wp_get_attachment_url($at['logo']),
		"menu" => clean_menu($menu)
	];

	ob_start();
	?>

	<div
    class="header-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'header', 'header_sc' );