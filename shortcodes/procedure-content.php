<?php
include_once str_replace('shortcodes', '', __DIR__) . 'lib/countries.php';
include_once str_replace('shortcodes', '', __DIR__) . 'lib/location.php';

function procedure_content_sc( $atts ){

	$at = shortcode_atts([
    'select_country' => gett('Seleccionar país'),
		'city' => gett('Ciudad'),
		'name' => gett('Nombre'),
		'lastname' => gett('Apellido'),
		'email' => gett('Email'),
    'btn' => gett('Cotizar'),
    'form_text' => gett('Cotiza este procedimiento')
  ], $atts);

	$props = [
    "countries" => getCountries(),
		"country" => getCountry(),
    "trans" => [
      "name" => $at['name'],
			"email" => $at['email'],
			"select_country" => $at['select_country'],
			"city" => $at['city'],
      "btn" => $at['btn'],
      'form_text' => $at['form_text']
    ]
  ];

	ob_start();
	?>

	<div
    class="procedure-content-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'procedure_content', 'procedure_content_sc' );