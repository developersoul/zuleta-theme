<?php

function newsletter_form_sc( $atts ){

	$at = shortcode_atts([
		'email' => gett('Email'),
		'invalid_email' => gett('Email invalido'),
		'subscribed' => gett('Subscrito'),
		'subscribe' => gett('SUSCRÍBETE')
	], $atts);

	$props = [
		"trans" => [
			"email" => $at['email'],
			"invalid_email" => $at['invalid_email'],
			"subscribed" => $at['subscribed'],
			"subscribe" => $at['subscribe'],
		]
	];

	ob_start();
	?>

	<div
    class="newsletter-form-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'newsletter_form', 'newsletter_form_sc' );