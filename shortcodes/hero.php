<?php

function hero_sc( $atts ){

	$at = shortcode_atts([
		'bg' => '',
	], $atts);

	$props = [
		"bg" => $at['bg']
	];

	ob_start();
	?>

	<div
    class="hero-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'hero', 'hero_sc' );