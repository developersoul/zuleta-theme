<?php
function posts_sc( $atts ){

	$at = shortcode_atts([
		'id' => '',
		'per_page' => 3,
		'paged' => 1,
		'categories' => '',
		'search' => '',
		'comments' => gett('comentarios'),
		'see_more' => gett('Ver más'),
		'show_pagination' => ''
	], $atts);


  $query = new Wp_Query(array(
			's' => $at['search'],
      'post_type' => array('post'),
			'posts_per_page' => $at['per_page'],
			'paged' => !empty($_GET['posts_page']) ? intval($_GET['posts_page']) : $at['paged'],
			'post_status' => 'publish',
			array( 'category_name' => $at['categories'] )
		));

  $posts = array_map(function($post) {
		$images = !empty(get_post_meta($post->ID, 'image_square_key', true)) ? get_post_meta($post->ID, 'image_square_key', true) : '';
    $post->post_image = get_the_post_thumbnail_url($post->ID);
		$content = substr($post->post_content, 0, 250) ? substr($post->post_content, 0, 250) : $post->post_content;
		$post->post_short = substr(preg_replace('/\[(.*?)\]/', '', wp_strip_all_tags($post->post_content)), 0, 200);
		$post->post_content = '';
		$post->post_permalink = get_post_permalink($post->ID);
		$post->post_date_formated = get_the_date( 'd-m-Y', $post->ID );
		$post->post_categories = get_the_category($post->ID);
		$post->author_name = get_the_author_meta('first_name', $post->post_author) . " " . get_the_author_meta('last_name', $post->post_author);
		$post->comments_count = wp_count_comments($post->ID);
		return $post;
	}, $query->get_posts());

  $props = [
		"posts" => $posts,
		"dir_uri" => get_template_directory_uri(),
		"trans" => [
			"see_more" => $at['see_more'],
			"comments" => $at['comments']
		]
	];

  ob_start();
	?>

	<div
    class="posts-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php if(count($posts) == 0): ?>
	<div class="page-title" style="display: block; padding: 60px; background: #f1f1f1; margin-bottom: 30px">
		<h3>Sin resultados</h3>
	</div>
	<?php endif; ?>

	<?php if($at['show_pagination'] == 'true'): ?>
		<div class="pagination-container">
			<div class="pagination">
				<?php if(isset($_GET['posts_page']) && intval($_GET['posts_page']) > 1): ?>
					<a href="?posts_page=<?php echo $_GET['posts_page'] - 1 ?>" class="btn" ><?php echo gett('Atrás') ?></a>
				<?php endif; ?>
				<?php if(count($posts) > 0): ?>
				<a href="?posts_page=<?php echo isset($_GET['posts_page']) ? $_GET['posts_page'] + 1 : 2 ?>" class="btn"><?php echo gett('Siguiente') ?></a>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<?php

	return ob_get_clean();
};

add_shortcode( 'posts', 'posts_sc' );