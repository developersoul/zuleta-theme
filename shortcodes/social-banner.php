<?php

function social_banner_sc( $atts ){

	$at = shortcode_atts([
		'bg' => '',
	], $atts);

	$props = [
		"bg" => $at['bg']
	];

	ob_start();
	?>

	<div
    class="social-banner-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'social_banner', 'social_banner_sc' );