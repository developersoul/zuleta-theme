<?php

function comment_form_sc( $atts ){

	$at = shortcode_atts([
		'comment' => gett('Comentario'),
		'name' => gett('Nombre'),
		'email' => gett('Email'),
    'post_comment' => gett('Comentar'),
    'comment_send' => gett('Comentario enviado')
	], $atts);

	$props = [
		"trans" => [
      'comment' => $at['comment'],
      'name' => $at['name'],
      'email' => $at['email'],
      'post_comment' => $at['post_comment'],
      'comment_send' => $at['comment_send']
    ]
	];

	ob_start();
	?>

<div
  class="comment-form-container"
  data-props='<?php echo wp_json_encode($props); ?>'
></div>

<?php
  return ob_get_clean();
}

add_shortcode( 'comment_form', 'comment_form_sc' );
