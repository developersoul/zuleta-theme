<?php

function procedure_item_sc( $atts ){

	$at = shortcode_atts([
		'title' => '',
		'image_url' => '',
		'url' => '',
		'categories' => '',
	], $atts);

	$props = [
		"title" => $at['title'],
		"url" => $at['url'],
		"image_url" => wp_get_attachment_url($at['image_url']),
		"categories" => vc_param_group_parse_atts($at['categories'])
	];

	ob_start();
	?>

	<div
    class="procedure-item-container"
    data-props='<?php echo wp_json_encode($props); ?>'
  ></div>

	<?php

	return ob_get_clean();
};

add_shortcode( 'procedure_item', 'procedure_item_sc' );