<?php

function testimonials_sc( $atts ){
	$at = shortcode_atts([
    'testimonial' => ''
	], $atts);

    $testimonials = array_map(function($slide){
      $slide['image'] = wp_get_attachment_url($slide['image']);
      return $slide;
    }, vc_param_group_parse_atts($at['testimonial']));


	$props = [
    'testimonials' => $testimonials
    ];

	ob_start();
	?>
    <section
    	class="testimonials-container"
    	data-props='<?php echo wp_json_encode($props) ?>'
    ></section>

	<?php

	return ob_get_clean();
}

add_shortcode( 'testimonials', 'testimonials_sc' );