<?php

function slider_vc() {

	$subparams = [
		[
			"type" => "attach_image",
			"heading" => "Image",
			"param_name" => "image"
		]
	];

	$params = [
    [
      'type' => 'param_group',
      'value' => '',
      'param_name' => 'slide',
      'params' => $subparams
    ],
    [
      'type' => 'textfield',
      'heading' => 'slides to show',
      'param_name' => 'slides_to_show'
    ],
    [
      'type' => 'textfield',
      'heading' => 'speed autoplay',
      'param_name' => 'speed',
      'value' => '3000'
    ]
	];

  vc_map(
    array(
      "name" =>  "Slider",
      "base" => "slider",
      "category" =>  "CZB",
      "params" => $params
    )
  );
};

add_action( 'vc_before_init', 'slider_vc' );