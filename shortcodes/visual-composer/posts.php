<?php

function posts_vc() {
	$params = [
    [
      'type' => 'textfield',
      'heading' => 'posts to show',
      'value' => 3,
      'param_name' => 'per_page',
    ],
    [
      'type' => 'checkbox',
      'heading' => 'pagination',
      'param_name' => 'show_pagination'
    ]
	];

	vc_map(
    [
      "name" =>  "Posts",
      "base" => "posts",
      "category" =>  "CZB",
      "show_settings_on_create" => false,
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'posts_vc' );