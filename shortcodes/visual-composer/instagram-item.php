<?php

function instagram_item_vc() {
	$params = [
		[
			'type' => 'textfield',
			'heading' => 'Instagram photo id',
			'param_name' => 'id',
		]
	];

	vc_map(
    [
      "name" =>  "Instagram photo",
      "base" => "instagram_item",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'instagram_item_vc' );