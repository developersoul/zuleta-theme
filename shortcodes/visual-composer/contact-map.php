<?php

function contact_map_vc() {
	$params = [
		[
			'type' => 'textfield',
			'heading' => 'Ciudad',
			'param_name' => 'city',
		],
		[
			'type' => 'textfield',
			'heading' => 'Dirección',
			'param_name' => 'address',
		],
		[
			'type' => 'textfield',
			'heading' => 'Teléfono',
			'param_name' => 'phone',
		],
		[
			'type' => 'textfield',
			'heading' => 'Botón',
			'param_name' => 'cta_text',
		],
		[
			'type' => 'textfield',
			'heading' => 'Latitud',
			'param_name' => 'lat',
		],
		[
			'type' => 'textfield',
			'heading' => 'Longitud',
			'param_name' => 'lng',
		]
	];

	vc_map(
    [
      "name" =>  "Contact Map",
      "base" => "contact_map",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'contact_map_vc' );