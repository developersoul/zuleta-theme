<?php

function newsletter_form_vc() {
	$params = [
		// [
		// 	'type' => 'textfield',
		// 	'heading' => 'Seleccionar País',
		// 	'param_name' => 'select_country'
		// ],
		// [
		// 	'type' => 'textfield',
		// 	'heading' => 'Ciudad',
		// 	'param_name' => 'city',
		// ],
		[
			'type' => 'textfield',
			'heading' => 'Correo',
			'param_name' => 'email',
		]
	];

	vc_map(
    [
      "name" =>  "Newsletter form",
      "base" => "newsletter_form",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'newsletter_form_vc' );