<?php

function procedure_content_vc() {
	$params = [
    [
			'type' => 'textfield',
			'heading' => 'form text',
      'param_name' => 'form_text',
    ],
    [
			'type' => 'textfield',
			'heading' => 'Seleccionar País',
			'param_name' => 'select_country'
		],
		[
			'type' => 'textfield',
			'heading' => 'Ciudad',
			'param_name' => 'city',
		],
		[
			'type' => 'textfield',
			'heading' => 'Nombre',
			'param_name' => 'name',
		],
		[
			'type' => 'textfield',
			'heading' => 'Correo',
      'param_name' => 'email',
    ],

	];

	vc_map(
    [
      "name" =>  "Procedure content",
      "base" => "procedure_content",
      "category" =>  "CZB",
      'params' => $params
		]
	);
}

add_action( 'vc_before_init', 'procedure_content_vc' );