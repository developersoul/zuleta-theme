<?php

function testimonials_vc() {

	$subparams = [
		[
			"type" => "attach_image",
			"heading" => "Image",
			"param_name" => "image"
    ],
    [
			'type' => 'textarea',
			'heading' => 'Content',
			'param_name' => 'text',
			'value' => ''
    ],
    [
			'type' => 'textfield',
			'heading' => 'Author',
			'param_name' => 'author',
			'value' => ''
		]
	];

	$params = [
    [
      'type' => 'param_group',
      'value' => '',
      'param_name' => 'testimonial',
      'params' => $subparams
    ]
	];

  vc_map(
    array(
      "name" =>  "Testimonials",
      "base" => "testimonials",
      "category" =>  "CZB",
      "params" => $params
    )
  );
};

add_action( 'vc_before_init', 'testimonials_vc' );