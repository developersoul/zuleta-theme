<?php

function slider_sc( $atts ){
	$at = shortcode_atts([
	'slide' => '',
	'slides_to_show' => '1',
	'speed' => '3000'
	], $atts);

	$slides = array_map(function($slide){
		$slide['image'] = wp_get_attachment_url($slide['image']);
		return $slide;
	}, vc_param_group_parse_atts($at['slide']));

	$props = [
		'slides' => $slides,
		'slides_to_show' => $at['slides_to_show'],
		'speed' => $at['speed'],
	];

	ob_start();
	?>
    <section
    	class="slider-container"
    	data-props='<?php echo wp_json_encode($props) ?>'
    ></section>

	<?php

	return ob_get_clean();
}

add_shortcode( 'slider', 'slider_sc' );